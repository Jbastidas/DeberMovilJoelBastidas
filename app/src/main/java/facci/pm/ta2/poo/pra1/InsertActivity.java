package facci.pm.ta2.poo.pra1;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.SaveCallback;

public class InsertActivity extends AppCompatActivity {

    Button buttonCamara, buttonInsert;
    ImageView imageViewFoto;
    EditText editTextNombre, editTextPrecio, editTextDescripcion;
    Bitmap imagenBitmap;
    static final int REQUEST_IMAGE_CAPTURE =1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        buttonCamara = (Button) findViewById(R.id.buttonCamara);
        buttonInsert = (Button) findViewById(R.id.ButtonInsert);
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFoto);
        editTextNombre = findViewById(R.id.txtNombre);
        editTextPrecio=findViewById(R.id.txtPrecio);
        editTextDescripcion=findViewById(R.id.txtDescripcion);

        buttonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (tomarFoto.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(tomarFoto, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarImagen();
                final DataObject objeto = new DataObject("item");
                imageViewFoto.buildDrawingCache();
                Bitmap bmap = imageViewFoto.getDrawingCache();
                objeto.put("image", bmap);
                String nombre = editTextNombre.getText().toString();
                objeto.put( "name", nombre);
                String precio = editTextPrecio.getText().toString();
                objeto.put("price", precio);
                String descripcion = editTextDescripcion.getText().toString();
                objeto.put("description", descripcion);


                objeto.saveInBackground(new SaveCallback<DataObject>() {
                    @Override
                    public void done(DataObject object, DataException e) {
                        Toast.makeText(getApplicationContext(),"Inserción Correcta", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(InsertActivity.this, ResultsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Esconde el teclado cuando se da click en cualquier parte de la Activity que no sea un EditText
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }

    private static final int PERMISSION_SAVE=101;
    String direccion =  Environment.getExternalStorageDirectory().getAbsolutePath();

    private void GuardarImagen()    {
        //Preguntar si tenemos permisos a la SDCard
        String nombreFoto = String.valueOf(editTextNombre.getText());
        if(tienePermisoSDCard())        {
            //Crear Carpeta
            //Guardarc el archivo
            CrearCarpeta();
            direccion+="/Imagenes4B/";
            GuardarFoto(direccion, nombreFoto, imagenBitmap);
        }else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_SAVE
            );
        }
    }

    private void CrearCarpeta(){
        String nombreCarpeta = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "Imagenes4B";
        File carpeta =new File(nombreCarpeta);
        if(!carpeta.exists()){
            if (carpeta.mkdir()){
                Toast.makeText(this,"Carpeta Creada",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, " Carpeta no Creada",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void GuardarFoto(String direccion, String nombreFoto, Bitmap bitmap) {

        try {
            File foto = new File(direccion, nombreFoto);
            FileOutputStream stream = new FileOutputStream(foto);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException e) { }
    }

    private boolean tienePermisoSDCard(){
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode ==RESULT_OK){
            Bundle extras = data.getExtras();
            imagenBitmap = (Bitmap) extras.get("data");
            imageViewFoto.setImageBitmap(imagenBitmap);
        }
    }

}
